#pragma once
#include <string>


using namespace std;
class Monster;
class Occupation
{
public:
	Occupation(string name, string Class, int hp, int BaseHp, int power, int vit, int agi, int dex);
	
	void move();
	void rest();
	void attack(Monster* target);
	void getStats();
	void levelup();

	int setHp(int hp);

	int GainExp(Monster* target);
	string getName();
	int getHp();
	int getVit();
	int getAgi();
	int getDex();


private:
	string mName;
	string mClass;
	int mHp;
	int mBaseHp;
	int mPower;
	int mVit;
	int mAgi;
	int mDex;
	int mLevel;
	int mExp;
	int mExpToNextLevel;
	
};

