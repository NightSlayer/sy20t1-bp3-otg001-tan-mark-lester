#include <iostream>
#include <string>
#include <time.h>
#include "Occupation.h"
#include "Monster.h"

using namespace std;

Occupation* pickclass() {
	Occupation* player = NULL;
	int choice = 0;
	string name;
	cout << "Enter your character's name: ";
	cin >> name;
	
	while ((choice < 1) || (choice > 4)) {
		cout << "Choose your characters occupation" << endl;
		cout << " 1) Warrior   2)Archer   3)Wizard   4)Thief" << endl;
		cin >> choice;
		if (choice == 1) {
			player = new Occupation(name, "Warrior", 15, 15, 5, 5, 3, 3);
		}
		else if (choice == 2) {
			player = new Occupation(name, "Archer", 10, 10, 3, 4, 4, 4);
		}
		else if (choice == 3) {
			player = new Occupation(name, "Wizard", 10, 10, 6, 3, 3, 3);
		}
		else if (choice == 4) {
			player = new Occupation(name, "Thief", 11, 11, 4, 2, 4, 5);
		}
	}
	return player;
}

int playerPosition(int& x, int& y) {
	int choice = 0;
	while ((choice < 1) || (choice > 4)) {
		cout << "Which way would you want to go?" << endl;
		cout << "[1]North  [2]South  [3]East  [4]West" << endl;
		cin >> choice;
	}
	if (choice == 1) 
		x++;
	
	else if (choice == 2) 
		x--;
	
	else if (choice == 3) 
		y++;
	
	else if (choice == 4) 
		y--;
	return x, y;
}
Monster* spawning(bool spawned) {
	Monster* monster = NULL;
	int spawnrate= rand() % 100 + 1;
	if (spawnrate <= 20 && spawnrate >= 1) {
		spawned = false; 
	}
	else if (spawnrate <= 45 && spawnrate >= 21) {
		cout << "A Goblin has appeared" << endl;
		monster = new Monster("Goblin", 5, 3, 1, 2, 2, 50);
	}
	else if (spawnrate <= 70 && spawnrate >= 46) {
		cout << "A Orge has appeared" << endl;
		monster = new Monster("Orge", 10, 4, 3, 4, 3, 100);
	}
	else if (spawnrate <= 95 && spawnrate >= 71) {
		cout << "Orc was found" << endl;
		monster = new Monster("Orc", 15, 5, 4, 4, 4, 200);
	}
	else if (spawnrate <= 100 && spawnrate >= 96) {
		cout << "Orc Lord was found" << endl;
		monster = new Monster("Orc Lord", 20, 7, 5, 4, 3, 500);
	}
	return monster;
}
void battle(Monster*& monster, Occupation*& player) {
	while (true) {
		int escape = rand() % 100 + 1;
		int choice = 0;
		cout << "Your Hp: " << player->getHp() << endl;
		cout << monster->getName() << " Hp: " << monster->getHp() << endl;
		cout << "What do you do" << endl;
		cout << "[1] Attack  [2] Run" << endl;
		cin >> choice;
        
		if (choice == 1) {
			player->attack(monster);
			monster->attack(player);
			system("pause");
		}
		else {
			if (escape <=25) {
				cout << "Cannot Escape" << endl;
				monster->attack(player);
				system("pause");
			}
			else{
				cout << "Run Away Safely" << endl;
				system("pause");
				delete monster;
				break;
		    }
		}
	    
		if (monster->getHp() <= 0) {
			cout << "You defeated the " << monster->getName() << endl;
			cout << "You get " << monster->getExpGained() << " exp" << endl;
			system("pause");
			player->GainExp(monster);
			delete monster;
			break;
		}
		else if (player->getHp() <= 0)
			break;
		system("cls");
	}
    
 }
int menu(int& x, int& y) {
	srand(time(NULL));
	int choice;
	cout << "Your Position: ( " << x << ", " << y << ")" << endl;
	cout << "What do you want to do" << endl;
	cout << "[1]Walk  [2]Rest  [3]Get Stats  [4]Quit" << endl;
	cin >> choice;
	return choice;
	system("cls");
}
int main() {
	int choice = 0;
	int x=0;
	int y=0;
	Occupation* player = pickclass();
	while (true) {
		bool spawned = true;
		choice = menu(x, y);

		if (choice == 1) {
			playerPosition(x, y);
			Monster* monster = spawning(spawned);
			if (spawned == false) {
				cout << "Nothing was found" << endl;
			}
			else if(spawned==true) {
				battle(monster, player);
			}
			
		}
		else if (choice == 2){
			player->rest();
			cout << "Resting" << endl;
	    }
		else if (choice == 3) 
			player->getStats();
		
		else {
			cout<<"Bye Bye have a great time"<<endl;
			break;
	    }
		if (player->getHp() <= 0) {
			cout << "YOU DIED" << endl;
            system("pause");
			break;
		}
		system("pause");
		system("cls");
	}
	

	
}
