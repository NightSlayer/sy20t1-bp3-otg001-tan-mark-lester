#include "Occupation.h"
#include "Monster.h"
#include <iostream>
#include <string>
using namespace std;

Occupation::Occupation(string name, string Class, int hp, int BaseHp, int power, int vit, int agi, int dex)
{
	mName = name;
	mHp = hp;
	mClass = Class;
	mBaseHp = BaseHp;
	mPower = power;
	mVit = vit;
	mAgi = agi;
	mDex = dex;
	mLevel = 1;
	mExp = 0;
	mExpToNextLevel = mLevel * 1000;
	
}


void Occupation::move()
{
	cout << "moving" << endl;
}

void Occupation::rest()
{
	cout << "Resting..." << endl;
	mHp = mBaseHp;
}

void Occupation::attack(Monster* target)
{
	int randomhit = rand() % 80 + 20;
	int hitrate = ((float)mDex / (float)target->getAgi()) * 100;
	int damage = mPower - target->getVit();
	if (damage <= 0) damage = 1;
	if (hitrate >=80 || hitrate <=20) {
		target->setHp(target->getHp() - damage);
		cout << "You hit the " << target->getName() << " for " << damage << " damage" << endl;
	}
	else
	cout << "You missed" << endl;
}

void Occupation::getStats()
{
	cout << "Name:  " << mName << endl;
	cout << "Class:  " << mClass << endl;
	cout << "Level: " << mLevel << endl;
	cout << "HP:    " << mHp << "/" << mBaseHp << endl;
    cout << "Power:  " << mPower << endl;
	cout << "VIT:   " << mVit << endl;
	cout << "AGI:   " << mAgi << endl;
	cout << "Dex:   " << mDex << endl;
	cout << "EXP:   " << mExp << "/" << mExpToNextLevel << endl;
	system("pause");
	system("cls");
	
}

void Occupation::levelup()
{
	if (mExp >= mExpToNextLevel) {
		mBaseHp+= rand() % 4;
		mHp = mBaseHp;
		mPower+= rand() % 2;
		mVit+= rand() % 2;
		mAgi+= rand() % 2;
		mDex+= rand() % 2;
		mLevel++;
		mExp -= mExpToNextLevel;
		mExpToNextLevel* mLevel;
	}
}

int Occupation::setHp(int hp)
{
	return mHp = hp;
}

int Occupation::GainExp(Monster* target)
{
	return mExp += target->getExpGained();
}

string Occupation::getName()
{
	return mName;
}

int Occupation::getHp()
{
	return mHp;
}

int Occupation::getVit()
{
	return mVit;
}

int Occupation::getAgi()
{
	return mAgi;
}

int Occupation::getDex()
{
	return mDex;
}