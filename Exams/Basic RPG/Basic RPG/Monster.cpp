#include "Monster.h"
#include "Occupation.h"
#include <iostream>
#include <string>
using namespace std;

Monster::Monster(string name, int hp, int power, int vit, int agi, int dex, int ExpGained)
{
	mName = name;
	mHp = hp;
	mVit = vit;
	mPower = power;
	mAgi = agi;
	mDex = dex;
	mExpGained = ExpGained;
	
}

void Monster::attack(Occupation* target)
{
	int randomhit = rand() % 80 + 20;
	int hitrate = ((float)mDex / (float)target->getAgi()) * 100;
	int damage = mPower - target->getVit();
	if (damage <= 0) damage = 1;
	if (hitrate >= 80 || hitrate <= 20) {
		target->setHp(target->getHp() - damage);
		cout << "The " << mName << " hit you for " << damage << " damage" << endl;
	}
	else
		cout << "The "<< mName <<" missed" << endl;
}

int Monster::setHp(int hp)
{
	return mHp = hp;
}


string Monster::getName()
{
	return mName;
}

int Monster::getHp()
{
	return mHp;
}

int Monster::getVit()
{
	return mVit;
}

int Monster::getAgi()
{
	return mAgi;
}

int Monster::getDex()
{
	return mDex;
}

int Monster::getExpGained()
{
	return mExpGained;
}
