#pragma once
#include <string>

using namespace std;

class Occupation;
class Monster
{
public:
	Monster(string name, int hp, int power, int vit, int agi, int dex, int ExpGained);
	
	void attack(Occupation* target);
	
	int setHp(int hp);

	string getName();
	int getHp();
	int getVit();
	int getAgi();
	int getDex();
	int getExpGained();

private:
	string mName;
	int mHp;
	int mVit;
	int mPower;
	int mAgi;
	int mDex;
	int mExpGained;
};

