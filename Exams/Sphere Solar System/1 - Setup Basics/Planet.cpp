#include "Planet.h"


Planet::Planet(SceneNode* node, SceneManager* mSceneMgr, int size, int r, int g, int b)
{
	parentnode = node;
	MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create("manualMaterial", "General");
	myManualObjectMaterial->getTechnique(0)->setLightingEnabled(true);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(1, 1, 1, 0);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setSpecular(0.3, 0.3, 0.3, 0);
	ManualObject* manual = mSceneMgr->createManualObject("");
	manual->begin("manualMaterial", RenderOperation::OT_TRIANGLE_LIST);

	//Front
	manual->position(-size, -size, size);
	manual->colour(r,g,b);
	manual->normal(Vector3(0, 0, 1));
	manual->position(size, -size, size);
	manual->colour(r, g, b);
	manual->normal(Vector3(0, 0, 1));
	manual->position(size, size, size);
	manual->colour(r, g, b);
	manual->normal(Vector3(0, 0, 1));
	manual->position(-size, size, size);
	manual->colour(r, g, b);
	manual->normal(Vector3(0, 0, 1));

	//Right
	manual->position(size, -size, -size);
	manual->colour(r, g, b);
	manual->normal(Vector3(1, 0, 0));
	manual->position(size, size, -size);
	manual->colour(r, g, b);
	manual->normal(Vector3(1, 0, 0));
	manual->position(size, size, size);
	manual->colour(r, g, b);
	manual->normal(Vector3(1, 0, 0));
	manual->position(size, -size, size);
	manual->colour(r, g, b);
	manual->normal(Vector3(1, 0, 0));

	//Left
	manual->position(-size, -size, size);
	manual->colour(r, g, b);
	manual->normal(Vector3(-1, 0, 0));
	manual->position(-size, size, size);
	manual->colour(r, g, b);
	manual->normal(Vector3(-1, 0, 0));
	manual->position(-size, size, -size);
	manual->colour(r, g, b);
	manual->normal(Vector3(-1, 0, 0));
	manual->position(-size, -size, -size);
	manual->colour(r, g, b);
	manual->normal(Vector3(-1, 0, 0));

	//top
	manual->position(-size, size, size);
	manual->colour(r, g, b);
	manual->normal(Vector3(0, 1, 0));
	manual->position(size, size, size);
	manual->colour(r, g, b);
	manual->normal(Vector3(0, 1, 0));
	manual->position(size, size, -size);
	manual->colour(r, g, b);
	manual->normal(Vector3(0, 1, 0));
	manual->position(-size, size, -size);
	manual->colour(r, g, b);
	manual->normal(Vector3(0, 1, 0));

	//bottom
	manual->position(-size, -size, size);
	manual->colour(r, g, b);
	manual->normal(Vector3(0, -1, 0));
	manual->position(-size, -size, -size);
	manual->colour(r, g, b);
	manual->normal(Vector3(0, -1, 0));
	manual->position(size, -size, -size);
	manual->colour(r, g, b);
	manual->normal(Vector3(0, -1, 0));
	manual->position(size, -size, size);
	manual->colour(r, g, b);
	manual->normal(Vector3(0, -1, 0));

	//back
	manual->position(size, size, -size);
	manual->colour(r, g, b);
	manual->normal(Vector3(0, 0, -1));
	manual->position(size, -size, -size);
	manual->colour(r, g, b);
	manual->normal(Vector3(0, 0, -1));
	manual->position(-size, -size, -size);
	manual->colour(r, g, b);
	manual->normal(Vector3(0, 0, -1));
	manual->position(-size, size, -size);
	manual->colour(r, g, b);
	manual->normal(Vector3(0, 0, -1));
	

	manual->end();
	
    planetnode=mSceneMgr->getRootSceneNode()->createChildSceneNode();
	planetnode->attachObject(manual);
	
}

void Planet::update(const FrameEvent& evt, float DegreePerSecond, SceneNode* parentnode)
{
	
	Radian rotationAngle = Radian(Degree(30 * evt.timeSinceLastFrame));
	Degree revolutionDegrees = Degree(DegreePerSecond * evt.timeSinceLastFrame);
    planetnode->rotate(Vector3(0, -1, 0), rotationAngle);
	float oldX = planetnode->getPosition().x - parentnode->getPosition().x;
	float oldZ= planetnode->getPosition().z - parentnode->getPosition().z;
	float newX = (oldX * Math::Cos(Radian(revolutionDegrees))) + (oldZ * Math::Sin(Radian(revolutionDegrees)));
	float newZ = (oldX * -Math::Sin(Radian(revolutionDegrees))) + (oldZ * Math::Cos(Radian(revolutionDegrees)));

	planetnode->setPosition(newX, planetnode->getPosition().y, newZ);
	
}

void Planet::setPosition(int x, int y, int z)
{
	planetnode->translate(Vector3(x, y, z));
}

SceneNode* Planet::getnode()
{
	return planetnode;
}
