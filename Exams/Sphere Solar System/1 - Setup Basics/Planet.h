#pragma once
#include <OgreManualObject.h>
#include "BaseApplication.h"


using namespace Ogre;
class Planet
{
public:
	Planet(SceneNode* node, SceneManager* mSceneMgr ,int size, int r, int g, int b);

	void update(const FrameEvent& evt, float DegreePerSecond, SceneNode* parentnode);
	void setPosition(int x, int y, int z);
	SceneNode* getnode();

private:
	SceneNode* planetnode;
	SceneNode* parentnode;
	Vector3 velocity;
};

