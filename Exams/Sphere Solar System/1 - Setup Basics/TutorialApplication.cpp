/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	ManualObject* cube = createCube();
	cubenode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	cubenode->attachObject(cube);
	cubenode->translate(Vector3(0, 0, 0));

	sun = new Planet(planetnode, mSceneMgr, 20, 1, 0, 0);
	sun->getnode()->setPosition(0, 0, 0);
	
	mercury = new Planet(planetnode, mSceneMgr, 3, 1, 0, 1);
	mercury->getnode()->setPosition(20, 0, 0);
	
	venus= new Planet(planetnode, mSceneMgr, 5, 1, 0, 1);
	venus->getnode()->setPosition(30, 0, 0);

	earth= new Planet(planetnode, mSceneMgr, 10, 0, 1, 1);
	earth->getnode()->setPosition(50, 0, 0);

	moon= new Planet(planetnode, mSceneMgr, 1, 1, 1, 1);
	moon->getnode()->setPosition(65, 0, 0);

	mars= new Planet(planetnode, mSceneMgr, 8, 0, 0, 1);
	mars->getnode()->setPosition(75, 0, 0);
	
    
	
	Light *pointLight = mSceneMgr->createLight();
	pointLight->setType(Light::LightTypes::LT_POINT);
	pointLight->setPosition(Vector3(0, 0, 0));
	pointLight->setDiffuseColour(ColourValue(1.0f, 1.0f, 0.0f));
	pointLight->setSpecularColour(ColourValue(1.0f, 1.0f, 1.0f));
	pointLight->setAttenuation(325, 0.0f, 0.014, 0.0007);
	pointLight->setCastShadows(false);

	mSceneMgr->setAmbientLight(ColourValue(0.1f, 0.1f, 0.1f));
}
ManualObject* TutorialApplication::createCube()
{
	MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create("manualMaterial", "General");
	myManualObjectMaterial->getTechnique(0)->setLightingEnabled(true);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(1, 1, 1, 0);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setSpecular(0.3, 0.3, 0.3, 0);
	ManualObject* manual = mSceneMgr->createManualObject("");
	manual->begin("manualMaterial", RenderOperation::OT_TRIANGLE_LIST);
	
	//Front
	manual->position(-10.0, -10.0, 10.0);
	manual->colour(ColourValue::White);
	manual->normal(Vector3(0, 0, 1));
	manual->position(10.0, -10.0, 10.0);
	manual->normal(Vector3(0, 0, 1));
	manual->position(10.0, 10.0, 10.0);
	manual->normal(Vector3(0, 0, 1));
	manual->position(-10.0, 10.0, 10.0);
	manual->normal(Vector3(0, 0, 1));

	//Right
    manual->position(10.0, -10.0, -10.0);
	manual->normal(Vector3(1, 0, 0));
    manual->position(10.0, 10.0, -10.0);
	manual->normal(Vector3(1, 0, 0));
	manual->position(10.0, 10.0, 10.0);
	manual->normal(Vector3(1, 0, 0));
	manual->position(10.0, -10.0, 10.0); 
	manual->normal(Vector3(1, 0, 0));

	//Left
	manual->position(-10.0, -10.0, 10.0);
	manual->normal(Vector3(-1, 0, 0));
	manual->position(-10.0, 10.0, 10.0);
	manual->normal(Vector3(-1, 0, 0));
    manual->position(-10.0, 10.0, -10.0);
	manual->normal(Vector3(-1, 0, 0));
	manual->position(-10.0, -10.0, -10.0);
	manual->normal(Vector3(-1, 0, 0));

	//top
	manual->position(-10.0, 10.0, 10.0);
	manual->normal(Vector3(0, 1, 0));
	manual->position(10.0, 10.0, 10.0);
	manual->normal(Vector3(0, 1, 0));
	manual->position(10.0, 10.0, -10.0);
	manual->normal(Vector3(0, 1, 0));
	manual->position(-10.0, 10.0, -10.0); 
	manual->normal(Vector3(0, 1, 0));

	//bottom
	manual->position(-10.0, -10.0, 10.0);
	manual->normal(Vector3(0, -1, 0));
	manual->position(-10.0, -10.0, -10.0);
	manual->normal(Vector3(0, -1, 0));
	manual->position(10.0, -10.0, -10.0);
	manual->normal(Vector3(0, -1, 0));
	manual->position(10.0, -10.0, 10.0);
	manual->normal(Vector3(0, -1, 0));

	//back
	manual->position(10.0, 10.0, -10.0);
	manual->normal(Vector3(0, 0, -1));
	manual->position(10.0, -10.0, -10.0);
	manual->normal(Vector3(0, 0, -1));
	manual->position(-10.0, -10.0, -10.0);
	manual->normal(Vector3(0, 0, -1));
	manual->position(-10.0, 10.0, -10.0);
	manual->normal(Vector3(0, 0, -1));

	manual->end();
	return manual; 
}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	//movement and acceleration
	/*
	if (mKeyboard->isKeyDown(OIS::KC_I)) {
		velocity.y += (10 * evt.timeSinceLastFrame) * 5;
		cubenode->translate(0.0, velocity.y, 0.0);
	} 
	if (mKeyboard->isKeyDown(OIS::KC_K)) {
		velocity.y -= (10 * evt.timeSinceLastFrame) * 5;
		cubenode->translate(0.0, velocity.y , 0.0);
	}
	if (mKeyboard->isKeyDown(OIS::KC_J)) {
		velocity.x -= (10 * evt.timeSinceLastFrame) * 5;
		cubenode->translate(velocity.x, 0.0, 0.0);
	}
	if (mKeyboard->isKeyDown(OIS::KC_L)) {
		velocity.x += (10 * evt.timeSinceLastFrame) * 5;
		cubenode->translate(velocity.x, 0.0, 0.0);
	}
	if(!(mKeyboard->isKeyDown(OIS::KC_I)|| mKeyboard->isKeyDown(OIS::KC_K) || mKeyboard->isKeyDown(OIS::KC_J) || mKeyboard->isKeyDown(OIS::KC_L))){
	       velocity=Vector3::ZERO;
	}
*/
	//Rotation
	
	/*Radian rotationAngle = Radian(Degree(30 * evt.timeSinceLastFrame));
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8)) {
		cubenode->rotate(Vector3(1, 0, 0), rotationAngle);
	}
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD2)) {
		cubenode->rotate(Vector3(-1, 0, 0), rotationAngle);
	}
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD4)) {
		cubenode->rotate(Vector3(0 , -1, 0), rotationAngle);
	}
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD6)) {
		cubenode->rotate(Vector3(0, 1, 0), rotationAngle);
	}*/
	//Revolution
	/*Degree revolutionDegrees = Degree(30 * evt.timeSinceLastFrame);
	float newX = (cubenode->getPosition().x * Math::Cos(Radian(revolutionDegrees))) - (cubenode->getPosition().y * Math::Sin(Radian(revolutionDegrees)));
	float newY= (cubenode->getPosition().x * Math::Sin(Radian(revolutionDegrees))) + (cubenode->getPosition().y * Math::Cos(Radian(revolutionDegrees)));
	
	cubenode->setPosition(newX, newY, cubenode->getPosition().z); */
	
	sun->update(evt, 0, sun->getnode());
	mercury->update(evt, 25, sun->getnode());
	venus->update(evt, 10, sun->getnode());
	earth->update(evt, 6, sun->getnode());
	moon->update(evt, 30, earth->getnode());
	mars->update(evt, 3, sun->getnode());
	
	return true;
	
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
