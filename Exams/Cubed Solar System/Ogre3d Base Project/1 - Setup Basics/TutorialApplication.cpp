/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	/*ManualObject* cube = createCube();
	cubenode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	cubenode->attachObject(cube);
	cubenode->translate(Vector3(0, 0, 0));*/
	
	sun = new Planet(planetnode, mSceneMgr, 20, 1, 0, 0);
	sun->getnode()->setPosition(0, 0, 0);
	
	mercury = new Planet(planetnode, mSceneMgr, 3, 1, 0, 1);
	mercury->getnode()->setPosition(20, 0, 0);
	
	venus= new Planet(planetnode, mSceneMgr, 5, 1, 0, 1);
	venus->getnode()->setPosition(30, 0, 0);

	earth= new Planet(planetnode, mSceneMgr, 10, 0, 1, 1);
	earth->getnode()->setPosition(50, 0, 0);

	moon= new Planet(planetnode, mSceneMgr, 1, 1, 1, 1);
	moon->getnode()->setPosition(65, 0, 0);

	mars= new Planet(planetnode, mSceneMgr, 8, 0, 0, 1);
	mars->getnode()->setPosition(75, 0, 0);


}
ManualObject* TutorialApplication::createCube()
{
	ManualObject* manual = mSceneMgr->createManualObject("");
	manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	manual->position(-10.0, -10.0, 10.0);
	manual->colour(0, 0, 1);
	manual->position(10.0, -10.0, 10.0);
	manual->colour(0, 1, 0);
	manual->position(10.0, 10.0, 10.0);
	manual->colour(1, 0, 0);
	manual->position(-10.0, 10.0, 10.0);
	manual->colour(0, 0, 1);


	manual->position(-10.0, -10.0, -10.0);
	manual->colour(0, 1, 0);
	manual->position(10.0, -10.0, -10.0);
	manual->colour(0, 0, 1);
	manual->position(10.0, 10.0, -10.0);
	manual->colour(0, 1, 0);
	manual->position(-10.0, 10.0, -10.0);
	manual->colour(1, 0, 0);

	//front
	manual->index(0);
	manual->index(1);
	manual->index(2);
	manual->index(3);
	manual->index(0);
	manual->index(2);
	
	//right side
	manual->index(5);
	manual->index(2);
	manual->index(1);
	manual->index(5);
	manual->index(6);
	manual->index(2);
	
	//top
	manual->index(6);
	manual->index(3);
	manual->index(2);
	manual->index(3);
	manual->index(6);
	manual->index(7);
	
	//left side
	manual->index(3);
	manual->index(4);
	manual->index(0);
	manual->index(3);
	manual->index(7);
	manual->index(4);
	
	//back
	manual->index(6);
	manual->index(5);
	manual->index(4);
	manual->index(6);
	manual->index(4);
	manual->index(7);
	
	//bottom
	manual->index(1);
	manual->index(0);
	manual->index(5);
	manual->index(4);
	manual->index(5);
	manual->index(0);

	
	manual->end();
	return manual; 
}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	//movement and acceleration
	/*
	if (mKeyboard->isKeyDown(OIS::KC_I)) {
		velocity.y += (10 * evt.timeSinceLastFrame) * 5;
		cubenode->translate(0.0, velocity.y, 0.0);
	} 
	if (mKeyboard->isKeyDown(OIS::KC_K)) {
		velocity.y -= (10 * evt.timeSinceLastFrame) * 5;
		cubenode->translate(0.0, velocity.y , 0.0);
	}
	if (mKeyboard->isKeyDown(OIS::KC_J)) {
		velocity.x -= (10 * evt.timeSinceLastFrame) * 5;
		cubenode->translate(velocity.x, 0.0, 0.0);
	}
	if (mKeyboard->isKeyDown(OIS::KC_L)) {
		velocity.x += (10 * evt.timeSinceLastFrame) * 5;
		cubenode->translate(velocity.x, 0.0, 0.0);
	}
	if(!(mKeyboard->isKeyDown(OIS::KC_I)|| mKeyboard->isKeyDown(OIS::KC_K) || mKeyboard->isKeyDown(OIS::KC_J) || mKeyboard->isKeyDown(OIS::KC_L))){
	       velocity=Vector3::ZERO;
	}
*/
	//Rotation
	/*
	Radian rotationAngle = Radian(Degree(30 * evt.timeSinceLastFrame));
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8)) {
		cubenode->rotate(Vector3(1, 0, 0), rotationAngle);
	}
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD2)) {
		cubenode->rotate(Vector3(-1, 0, 0), rotationAngle);
	}
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD4)) {
		cubenode->rotate(Vector3(0 , -1, 0), rotationAngle);
	}
	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD6)) {
		cubenode->rotate(Vector3(0, 1, 0), rotationAngle);
	}*/
	//Revolution
	/*Degree revolutionDegrees = Degree(30 * evt.timeSinceLastFrame);
	float newX = (cubenode->getPosition().x * Math::Cos(Radian(revolutionDegrees))) - (cubenode->getPosition().y * Math::Sin(Radian(revolutionDegrees)));
	float newY= (cubenode->getPosition().x * Math::Sin(Radian(revolutionDegrees))) + (cubenode->getPosition().y * Math::Cos(Radian(revolutionDegrees)));
	
	cubenode->setPosition(newX, newY, cubenode->getPosition().z); */
	
	sun->update(evt, 0, sun->getnode());
	mercury->update(evt, 25, sun->getnode());
	venus->update(evt, 10, sun->getnode());
	earth->update(evt, 6, sun->getnode());
	moon->update(evt, 30, earth->getnode());
	mars->update(evt, 3, sun->getnode());
	return true;
	
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
