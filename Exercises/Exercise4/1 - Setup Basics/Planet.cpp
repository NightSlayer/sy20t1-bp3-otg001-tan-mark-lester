#include "Planet.h"


Planet::Planet(SceneNode* node, SceneManager* mSceneMgr, int size, int r, int g, int b)
{
	parentnode = node;
	ManualObject* manual = mSceneMgr->createManualObject("");
	manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	

	manual->position(-size, -size, size);
	manual->colour(r, g, b);
	manual->position(size, -size, size);
	manual->colour(r, g, b);
	manual->position(size, size, size);
	manual->colour(r, g, b);
	manual->position(-size, size, size);
	manual->colour(r, g, b);


	manual->position(-size, -size, -size);
	manual->colour(r, g, b);
	manual->position(size, -size, -size);
	manual->colour(r, g, b);
	manual->position(size, size, -size);
	manual->colour(r, g, b);
	manual->position(-size, size, -size);
	manual->colour(r, g, b);

	//front
	manual->index(0);
	manual->index(1);
	manual->index(2);
	manual->index(3);
	manual->index(0);
	manual->index(2);

	//right side
	manual->index(5);
	manual->index(2);
	manual->index(1);
	manual->index(5);
	manual->index(6);
	manual->index(2);

	//top
	manual->index(6);
	manual->index(3);
	manual->index(2);
	manual->index(3);
	manual->index(6);
	manual->index(7);

	//left side
	manual->index(3);
	manual->index(4);
	manual->index(0);
	manual->index(3);
	manual->index(7);
	manual->index(4);

	//back
	manual->index(6);
	manual->index(5);
	manual->index(4);
	manual->index(6);
	manual->index(4);
	manual->index(7);

	//bottom
	manual->index(1);
	manual->index(0);
	manual->index(5);
	manual->index(4);
	manual->index(5);
	manual->index(0);

	manual->end();
	
    planetnode=mSceneMgr->getRootSceneNode()->createChildSceneNode();
	planetnode->attachObject(manual);
	
}

void Planet::update(const FrameEvent& evt, float DegreePerSecond, SceneNode* parentnode)
{
	
	Radian rotationAngle = Radian(Degree(30 * evt.timeSinceLastFrame));
	Degree revolutionDegrees = Degree(DegreePerSecond * evt.timeSinceLastFrame);
    planetnode->rotate(Vector3(0, -1, 0), rotationAngle);
	float oldX = planetnode->getPosition().x - parentnode->getPosition().x;
	float oldZ= planetnode->getPosition().z - parentnode->getPosition().z;
	float newX = (oldX * Math::Cos(Radian(revolutionDegrees))) + (oldZ * Math::Sin(Radian(revolutionDegrees)));
	float newZ = (oldX * -Math::Sin(Radian(revolutionDegrees))) + (oldZ * Math::Cos(Radian(revolutionDegrees)));

	planetnode->setPosition(newX, planetnode->getPosition().y, newZ);
	
}

void Planet::setPosition(int x, int y, int z)
{
	planetnode->translate(Vector3(x, y, z));
}

SceneNode* Planet::getnode()
{
	return planetnode;
}
